
all : pres.pdf handout.pdf

.PRECIOUS: %.md.tex %.pdf

pres.pdf : pres.md template.tex Makefile
	 pandoc --slide-level=2 -V documentclass=beamer -s $< -t beamer -f markdown+raw_tex -o $@ --template=template.tex

handout.pdf : handout.md Makefile
	 pandoc -s $< -o $@ -V geometry:a4paper,margin=3.5cm
