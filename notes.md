## Prikazy:

* Marek: init, add/mv/rm, diff, status, commit (amending), clone, pull/push, branch
* Vlada: ukazat vse predchozi, dale: checkout, log, grep, show, rebase, merge,
  (reset, revert), cherry-pick, stash (automatic stash with pull),
* struktura commit zpravy
* tagging
* merge, rebase resolving conflicts
* rebase -i
* git add/commit --patch
* (blame, bisect)
* .gitignore

## Nastroje:

* gitkraken: https://www.gitkraken.com/
* tig (git-gui)
* gitk, qgit?
* gitlab (github)

# Notes:

* answer question: why is it better to use git than svn?
* do not track empty directories

* (git branch -M  -- rename a branch

* advanced: git hooks -- found in .git/hooks
           executable scripts in that directory, triggered by various git commands
	  (commit, push, pre-commit, post-update)

* best practices:
  - Don't change published history
  - Document what you did in commit messages
  - Use branches for changes (master is for publishing)
                  

## References

* book: https://git-scm.com/book/en/v2
* cheatsheet: https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
* https://sethrobertson.github.io/GitBestPractices/

