---
vim: spell spelllang=cs
---

# GIT cheetsheet

## Vytvoření repozitáře

-   přes github/gitlab + `git clone URL`
*   ručně:
    *   `git init`
    *   `git commit` -- vytvoření commitu
    *   a zveřejnění:
    *   `git remote add origin URL` -- přidání URL k publikaci
    *   `git push --set-upstream origin master` -- první push

## Základní operace

*   `git pull [--rebase]`
*   `git add FILE` / `git add --patch`
*   `git commit [--patch]`
*   `git push`
*   `git status`
*   `git log`

## Jak na konflikty

*   sleduj, co `git` chce, správa o konfliktu obsahuje i návod jak ho vyřešit
*   sleduj `git status`
*   lokální změny v souboru lze odstranit pomocí `git checkout FILE`

## Tutoriál 1: základy + `pull` konflikt

#.  nový repozitář na gitlabu
#.  clone repozitáře k sobě
#.  vytvořte soubor, podívejte se na `git status`
#.  přidejte jen pomocí `git add`, znovu se podívejte na `git status`
#.  vytvořte commit a pošlete ho na server, podívejte se na `git log` a `tig`,
případně `gitk`
#.  přidejte člověku sedícímu vedle vás právo zapisovat do vašeho repozitáře a
počkejte až vám dá přístup do svého (práva se přidělují přes gitlab)
#.  stáhněte si repozitář vašeho souseda
#.  změňte jeho soubor, podívejte se na `git status` a `git diff`
#.  proveďte `commit` + `push` těchto změn
#.  jděte do svého repozitáře, nestahujte změny, změňte svůj soubor, proveďte
`commit` změn
#.  udělejte `pull`, git vám oznámí konflikt
#.  odstraňte konflikt editací souboru dle libosti
#.  pomocí `git add` přidejte změny v souboru
#.  spusťte `git commit`: vytvoří se merge commit
#.  podívejte se na log, zveřejněte merge commit

## Tutoriál 2: `pull --rebase` konflikt

#.  stáhněte si aktuální verzi sousedova repozitáře
#.  opět změňte jeho soubor a změny zveřejněte
#.  jděte do svého repozitáře, změňte svůj soubor a udělejte commit
#.  spusťte `git pull --rebase`
#.  editujte konflikt, spusťte `git add FILE`
#.  `git rebase --continue`
#.  podívejte se na log, zveřejněte

## Tutoriál 3: stash, branching

#.  stáhněte si repozitář `paradise/git-fakos-demo`
#.  jděte do větve `test`
#.  změňte soubor `test-b`, nevytvářejte commit
#.  vyčkejte na pokyn k pokračování
#.  zkuste udělat git pull
#.  proveďte stash současných změn (`git stash push`)
#.  zkuste se podívat na `git stash list`
#.  stáhněte změny
#.  aplikujte stash pomocí `git stash pop`

\par

10.  vytvořte si novou větev `test-VASE_JMENO` odvozenou od test
#.  změňte soubor `test-a`, proveďte commit
#.  znovu změňte `test-a` a přidejte změny k předchozímu commitu pomocí `git
commit --amend`
#.  změňte soubor `test-b` a vytvořte commit pro tuto změnu
#.  změňte znovu soubor `test-a` a proveďte commit
#.  vytvořte větev `test2-VASE_JMENO` odvozený od předchozí větve
#.  podívejte se na historii
#.  proveďte interaktivní rebase aktuální větve na master: `git rebase --interactive master` s následujícími změnami:
    *   spojte obě změny na `test-a` do jednoho commitu
    *   přidejte další změnu ke commitu, který mění `test-b`
#.  podívejte se na historii


## Demo: historie, vyhledávání

*   `git clone https://github.com/mchalupa/dg`
*   vyzkoušíme si `git log`, `tig`, `gitk`
*   `git grep`
*   `git tag -l` + `git checkout TAG`
*   `git branch` a spol.
*   `git checkout --track origin/…`
