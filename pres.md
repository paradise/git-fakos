---
title: GIT
authors:
  - Marek Chalupa
  - Vladimír Štill
date: 15\. září 2017
header-includes:
    - \institute{FAKOS}
theme: metropolis
lang: czech
...


## Verzované složky / lokální verzovací systém
 * Jednoduché, ale neefektivní a nepraktické
 * Jak spolupracovat s ostatními lidmi?


 \begin{center}
 \vspace{3mm}
 \includegraphics[width=6cm]{folders.png}
 \end{center}



## Centrální verzovací systém
 * Server je bottle-neck
 * Co když není internetové spojení?


 \begin{center}
 \includegraphics[width=6cm]{centralized.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/centralized.png}}}
 \end{center}

## Distribuovaný verzovací systém
 * Každý počítač (klient i server) má celou kopii repozitáře

 \begin{center}
 \includegraphics[width=5cm]{distributed.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/distributed.png}}}
 \end{center}

## GIT
 * Distribuovaný verzovací systém
 * Většina operací jsou lokální
 * Snímky souborového systému ("snapshot"), neukládá rozdíly
 * "Commit often, perfect later, publish once."

## GIT -- pozadí
 * Git sleduje danou složku a soubory v ní
 * Soubor je buď sledovaný nebo nesledovaný (tracked/untracked)
 * Každý sledovaný soubor může být ve třech stavech:
     - unmodified / modified / staged (indexed)

 \begin{center}
 \includegraphics[width=6cm]{lifecycle.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/lifecycle.png}}}
 \end{center}

## První tři příkazy
 * `git init` -- initializuj repozitář v prázdné složce
 * `git add`  -- přidej obsah do následujícího commitu
 * `git commit` -- vytvoř commit

 \begin{center}
 \includegraphics[width=6cm]{areas.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/areas.png}}}
 \end{center}

## Prohlížení změn a historie
 * `git log`
 * GUI nástroje: `tig`, `gitk`, `gitkraken`
 * Zkusíte si s Vláďou ;)

## Vrácení změn
 * `git reset HEAD` -- odznač indexované změny ("staged" se stane "modified")
 * `git checkout -- <soubor>` -- vrať všechny změny provedené na souboru
 * `git commit --amend` -- přidej indexovaný obsah do předchozího commitu

## Vzdálené repozitáře
 * `git clone` -- stáhni novou kopii vzdáleného repozitáře
 * `git pull` -- stáhni commity ze vzdáleného repozitáře
 * `git push` -- nahraj svoje commity do vzdáleného repozitáře


## Větve
 * Způsob jak bezpečně upravovat obsah bez rozbití "publikovaných" dat
 * Odštěpíme větev, provedeme změny, spojíme větve zpátky
 * Větvení je levné -- používejte často

 \begin{center}
 \includegraphics[width=7cm]{branches.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/advance-master.png}}}
 \end{center}

## Větve -- pokračování

 * `git checkout <nazev vetve>` -- přepínání mezi větvemi
 * `git branch <nazev vetve>` -- vytvoření nové větve
 * `git branch` -- vypsání větví


 * `git pull <repo> <vetev>` -- stáhni commity z vetvě repozitáře `repo`
 * `git push <repo> <vetev>` -- pošli commity do větve v repozitáři `repo`

## Větve -- spojování
 * `git merge <branch>` -- spoj větev `branch` do aktualní větve
 * Vytvoří se "merge commit"

 \begin{center}
 \includegraphics[height=3.5cm]{merge1.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/basic-merging-1.png}}}
 \end{center}

## Větve -- spojování
 * `git merge <branch>` -- spoj větev `branch` do aktualní větve
 * Vytvoří se "merge commit"

 \begin{center}
 \includegraphics[height=3.5cm]{merge2.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/basic-merging-2.png}}}
 \end{center}

## Větve -- spojování
 * Pokud je spojovaná větev přímým pokračováním aktuální větve,
   tak se nevytváří "merge commit" (provede se "fast-forward")
 * Jednotlivé commity se dají aplikovat pomocí `git cherry-pick`


## Větve -- rebase
 * Alternativa k `merge`
 * Přehrání změn (commitů) na danou větev

 \begin{center}
 \includegraphics[width=9cm]{rebase1.png}
 {\let\thefootnote\relax\footnote{{\tiny https://git-scm.com/book/en/v2/images/basic-rebase-3.png}}}
 \end{center}

## Větve -- rebase
 * `git rebase <branch>` -- na větev `branch` přehraj aktuální větev
 * `git rebase <branch1> <branch2>` -- na vět `branch1` přehraj větev `branch2`
 * `git rebase --onto <branch1> <branch2> <branch3>` -- na větev `branch1` přehraj
    větev `branch3`, ale až od místa, kde se odštěpila z `branch2`


## Tagování
 * Tag je (neměnný) ukazatel na určitý commit
 * Chovají se podobně jako větve (pull/push/checkout)
 * Může obsahovat dodatečné informace (kdo udělal tag, kdy, komentáře, ...) -- anotovaný tag

 * `git tag <nazev>`
 * `git tag -a <nazev> -m "komentar"`


## Dobré praktiky
 * Používat větve
 * Konzistentní malé commity (jeden commit = jedna logická změna)
 * Struktura commit zprávy -- další forma dokumentace
 * Neměnit již publikované commity
 * Nedělat rebase s commity, které existují mimo lokální adresář

## Zdroje

* Online kniha: https://git-scm.com/book/en/v2
* Cheatsheet: https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
* https://sethrobertson.github.io/GitBestPractices/
